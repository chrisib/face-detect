#include <iostream>
#include <opencv2/face.hpp>
#include <opencv2/core.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <facedetect.h>
#include <face.h>
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <tclap/CmdLine.h>
#include <pthread.h>
#include <unistd.h>

using namespace std;
using namespace cv;
using namespace TCLAP;
using namespace facedetect;

static string haarPath = "/usr/local/share/libface/haar";
static int cameraId = 0;

static pthread_mutex_t counterLock;
static pthread_mutex_t frameLock;

static int frameCounter = 0;
static double fps = 0;
static int frameHeight = 600;
static int frameWidth = 800;
static std::string outputFile = "";

static cv::Mat toDisplay;

/*!
 * \brief Approximate the camera's FPS while running the vision processing
 * \param arg Ignored
 * \return Always returns NULL
 */
static void *fpsMonitor(void *arg)
{
    (void)arg;
    for(;;) {
        sleep(5);
        pthread_mutex_lock(&counterLock);
        fps = (double)frameCounter/5.0;
        frameCounter = 0;
        pthread_mutex_unlock(&counterLock);
    }
    return NULL;
}

static void *writeVideo(void *arg)
{
    (void)arg;

    VideoWriter writer(outputFile,CV_FOURCC('D','I','V','X'),25,Size(frameWidth,frameHeight),true);

    for(;;)
    {
        pthread_mutex_lock(&frameLock);
        writer.write(toDisplay);
        pthread_mutex_unlock(&frameLock);
        usleep(40000);
    }

    return NULL;
}

/*!
 * \brief Parse command-line arguments, exit with code 1 if there is an error
 * \param argc The number of arguments
 * \param argv The values of the arguments
 */
static void parseArgs(int argc, const char *argv[])
{
    try {
        CmdLine cmd("Perform real-time face and facial feature detection", ' ',"1.0");

        ValueArg<string> haar("p","path","Path to the Haar files used for detection",false,haarPath,"string");
        cmd.add(haar);

        ValueArg<int> cam("d","device","Video device ID",false,cameraId,"int");
        cmd.add(cam);

        ValueArg<int> h("y","height","Frame height in pixels",false,frameHeight,"int");
        cmd.add(h);

        ValueArg<int> w("x","width","Frame width in pixels",false,frameWidth,"int");
        cmd.add(w);

        ValueArg<string> o("o","output","Specify an output file to record video to (divx/avi format)",false,outputFile,"file.avi");
        cmd.add(o);

        cmd.parse(argc,argv);

        if(haar.getValue().back() == '/') {
            cout << "Stripping trailing / character" << endl;
            haarPath = haar.getValue().substr(0,haar.getValue().size()-1);
        } else {
            haarPath = haar.getValue();
        }
        cameraId = cam.getValue();
        frameHeight = h.getValue();
        frameWidth = w.getValue();
        outputFile = o.getValue();
    } catch(ArgException &e) {
        cerr << "error: " << e.error() << " for argument " << e.argId() << endl;
        exit(1);
    }
}

/*!
 * \brief Grabs frames from the webcam and tries to find faces in them
 * \return Always returns 0
 */
int main(int argc, const char *argv[])
{
    parseArgs(argc,argv);

    pthread_mutex_init(&counterLock,NULL);
    pthread_mutex_init(&frameLock,NULL);
    frameCounter = 0;
    fps = 0;
    pthread_t fpsThread;
    pthread_create(&fpsThread,NULL,fpsMonitor,NULL);
    pthread_detach(fpsThread);

    char key = '\0';
    namedWindow("Face Detect");

    cout << "Opening /dev/video" << cameraId << endl;
    VideoCapture camera(cameraId);
    camera.set(CV_CAP_PROP_FRAME_HEIGHT,frameHeight);
    camera.set(CV_CAP_PROP_FRAME_WIDTH,frameWidth);

    cout << "Loading haar cascades from " << haarPath << endl;
    string faceHaar = haarPath+"/face.xml";
    string eyeHaar = haarPath+"/eye.xml";
    string noseHaar = haarPath+"/nose.xml";
    string mouthHaar = haarPath+"/mouth.xml";
    string glassesHaar = haarPath+"/glasses.xml";

    FaceDetect detector(faceHaar,eyeHaar,noseHaar,mouthHaar,glassesHaar);

    Mat fromCamera;
    UMat toProcess;
    vector<Face*> faces;
    vector<Rect_<int> > facePositions;
    pthread_t videoWriter;

    if(outputFile.length() > 0)
    {
        pthread_create(&videoWriter,NULL,writeVideo,NULL);
        pthread_detach(videoWriter);
    }

    do
    {
        if(camera.read(fromCamera))
        {
			if(fromCamera.rows != frameHeight || fromCamera.cols != frameWidth)
				resize(fromCamera,toProcess,Size(frameWidth,frameHeight));
			else
				fromCamera.copyTo(toProcess);

            detector.detectFaces(toProcess,facePositions,true);
            detector.detectFacialFeaures(toProcess,facePositions,faces,true);

            stringstream fpsTxt;
            pthread_mutex_lock(&counterLock);
            frameCounter++;
            fpsTxt << fps << " fps";
            pthread_mutex_unlock(&counterLock);

            for(unsigned int i=0; i<faces.size(); i++) {
                stringstream id;
                id << "Face " << (i+1);
                putText(toProcess,id.str(),faces[i]->position.tl(),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255),1);
            }

            //putText(toProcess,"Press Q to quit",Point(0,toDisplay.rows-8),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255),1);
            putText(toProcess,fpsTxt.str().c_str(),Point(0,8),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,255,255),1);
            putText(toProcess,"Complete Face",Point(toDisplay.cols-100,8),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(255,0,0),1);
            putText(toProcess,"Partial Face",Point(toDisplay.cols-100,16),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(0,255,255),1);
            putText(toProcess,"Corrupt Face",Point(toDisplay.cols-100,24),CV_FONT_HERSHEY_PLAIN,0.75,Scalar(0,0,255),1);

            pthread_mutex_lock(&frameLock);
            toProcess.copyTo(toDisplay);
            imshow("Face Detect",toDisplay);
            pthread_mutex_unlock(&frameLock);
            key = cvWaitKey(1);

            if(key == ' ')
            {
                time_t t;
                t = time(NULL);
                char *time = ctime(&t);
                char filename[strlen(time)+strlen(".png")+1];
                sprintf(filename,"%s.png",time);

                for(char *c = filename; *c!=0; c++)
                {
                    if(*c == ' ' || *c == '\t' || *c == '\n')
                        *c = '_';
                    else if(*c == ':')
                        *c = '.';
                }

                vector<int> params;
                params.push_back(CV_IMWRITE_PNG_COMPRESSION);
                params.push_back(80);
                pthread_mutex_lock(&frameLock);
                imwrite(filename,fromCamera,params);
                pthread_mutex_unlock(&frameLock);
                cout << "Saved image as " << filename << endl;
            }

            while(!faces.empty())
            {
                delete faces.back();
                faces.pop_back();
            }
            facePositions.clear();
        }
    } while(key != 'q' && key != 'Q');

    pthread_cancel(fpsThread);
    if(outputFile.length() > 0)
        pthread_cancel(videoWriter);
    return 0;
}
