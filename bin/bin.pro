TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = face-detect

QMAKE_CXXFLAGS += -std=c++11

OBJECTS_DIR = .build/

INCLUDEPATH += ../lib

LIBS += -lpthread -lopencv_core -lopencv_highgui -lopencv_face -lopencv_ml -lopencv_imgproc -lopencv_video -lopencv_videoio -lopencv_objdetect -lopencv_imgcodecs -L../lib -lface

SOURCES += main.cpp

HEADERS += 

INSTALLBASE = /usr/local

target.path     = $$INSTALLBASE/bin/
INSTALLS   += target
