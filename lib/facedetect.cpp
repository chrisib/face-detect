#include "facedetect.h"
#include "face.h"

using namespace facedetect;
using namespace std;
using namespace cv;

FaceDetect::FaceDetect(string faceCascadePath, string eyeCascadePath, string noseCascadePath, string mouthCascadePath, string glassesCascadePath)
{
    this->faceCascadePath = faceCascadePath;
    this->eyeCascadePath = eyeCascadePath;
    this->noseCascadePath = noseCascadePath;
    this->mouthCascadePath = mouthCascadePath;
    this->glassesCascadePath = glassesCascadePath;

    faceClassifier.load(this->faceCascadePath);
    eyeClassifier.load(this->eyeCascadePath);
    noseClassifier.load(this->noseCascadePath);
    mouthClassifier.load(this->mouthCascadePath);
    glassesClassifier.load(this->glassesCascadePath);
}

void FaceDetect::detectFaces(UMat& img, vector<Rect_<int> >& faces, bool draw)
{

    faceClassifier.detectMultiScale(img, faces, 1.15, 3, 0|CASCADE_SCALE_IMAGE, Size(30, 30));

    if(draw)
    {
        for(unsigned int i=0; i<faces.size(); i++)
        {
            Rect_<int> &r = faces[i];
            cv::rectangle(img,r.tl(),r.br(),CV_RGB(255,255,255),1);
        }
    }

    return;
}

void FaceDetect::detectFacialFeaures(UMat &img, const std::vector<cv::Rect_<int> > &faces, std::vector<Face*> &result, bool draw)
{
    for_each(faces.begin(),faces.end(),[&](const Rect_<int> &face){
        vector<Rect_<int> > eyes,nose,mouth;

        detectFacialFeaures(img,face,eyes,nose,mouth,false);

        UMat roi = img(face);

        Face *features = Face::create(roi,face,eyes,nose,mouth);
        if(features != NULL)
        {
            result.push_back(features);
            if(draw)
                features->draw(img,true,true,true);
        }
    });
}

void FaceDetect::detectFacialFeaures(UMat& img, const Rect_<int> &face,
                                     std::vector<cv::Rect_<int> > &eyes,
                                     std::vector<cv::Rect_<int> > &nose,
                                     std::vector<cv::Rect_<int> > &mouth,
                                     bool draw)
{
    // Mark the bounding box enclosing the face
    if(draw)
        rectangle(img, Point(face.x, face.y), Point(face.x+face.width, face.y+face.height),
            Scalar(255, 0, 0), 1, 4);

    // Eyes, nose and mouth will be detected inside the face (region of interest)
    UMat ROI = img(Rect(face.x, face.y, face.width, face.height));

    detectEyes(ROI, eyes);
    detectNose(ROI, nose);
    detectMouth(ROI, mouth);

    if(draw) {
        drawEyes(ROI,eyes);
        drawNose(ROI,nose);
        drawMouth(ROI,mouth);
    }
}

void FaceDetect::detectEyes(UMat& img, vector<Rect_<int> >& eyes)
{
    eyeClassifier.detectMultiScale(img, eyes, 1.20, 5, 0|CASCADE_SCALE_IMAGE, Size(30, 30));

    if(eyes.size() < 2) {
        eyes.clear();
        glassesClassifier.detectMultiScale(img, eyes, 1.20, 5, 0|CASCADE_SCALE_IMAGE, Size(30,30));
    }
}

void FaceDetect::detectNose(UMat& img, vector<Rect_<int> >& nose)
{
    noseClassifier.detectMultiScale(img, nose, 1.20, 5, 0|CASCADE_SCALE_IMAGE, Size(30, 30));
}

void FaceDetect::detectMouth(UMat& img, vector<Rect_<int> >& mouth)
{
    mouthClassifier.detectMultiScale(img, mouth, 1.20, 5, 0|CASCADE_SCALE_IMAGE, Size(30, 30));
}

void FaceDetect::drawEyes(UMat &img, std::vector<cv::Rect_<int> > &eyes)
{
    for(unsigned int i=0; i<eyes.size(); i++)
    {
        // Mark points corresponding to the centre of the eyes
        Rect &e = eyes[i];
        circle(img, Point(e.x+e.width/2, e.y+e.height/2), 3, Scalar(0, 255, 0), -1, 8);
    }
}

void FaceDetect::drawNose(UMat &img, std::vector<cv::Rect_<int> > &nose)
{
    for(unsigned int i=0; i<nose.size(); i++)
    {
        // mark the nose as a triangle
        Rect &n = nose[i];
        Point top(n.tl().x+n.width/2,n.tl().y);
        Point bl(n.tl().x,n.br().y);
        line(img,top,bl,Scalar(255,0,0),2);
        line(img,top,n.br(),Scalar(255,0,0),2);
        line(img,n.br(),bl,Scalar(255,0,0),2);
    }
}

void FaceDetect::drawMouth(UMat &img, std::vector<cv::Rect_<int> > &mouth)
{
    for(unsigned int i=0; i<mouth.size(); i++)
    {
        // mark the mouth as a rectangle with a line through the centre
        Rect &m = mouth[i];
        Point l(m.tl().x,m.tl().y+m.height/2);
        Point r(m.br().x,m.tl().y+m.height/2);
        line(img,l,r,Scalar(0,255,0),1);
        rectangle(img,m.tl(),m.br(),Scalar(0,255,0),2);
    }
}
