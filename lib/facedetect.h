/*! \mainpage libface.so -- OpenCV-derived face detection library
 *
 * \section intro_sec Overview
 *
 * libface is a library based on OpenCV 3.0.0 for performing face and facial feature detection.
 * The library uses Haar cascades to identify faces and features in an image, outputting the locations
 * of the face within the surrounding image and the locations of features (eyes, nose, mouth) within
 * the face.
 *
 * \section use_sec Usage
 *
 * To use libface in your application include the following headers:
 * \code{.cpp}
 * #include <facedetect/face.h>
 * #include <facedetect/facedetect.h>
 * \endcode
 *
 * Also add the library to your makefile/build command:
 * \code{.sh}
 * LIBS += -lface
 * \endcode
 *
 * To instantiate the face detector, use the FaceDetect constructor, providing paths to the Haar
 * cascade files for the features:
 * \code{.cpp}
 * facedetect::FaceDetect faceDetector("/path/to/face.xml","/path/to/eye.xml","/path/to/nose.xml","/path/to/mouth/xml","/path/to/glasses.xml");
 * \endcode
 *
 * To use the detector first acquire a cv::UMat of the image to process and pass it to the FaceDetect object:
 * \code{.cpp}
 * std::vector<cv::Rect> facePositions;
 * std::vector<facedetect::Face*> faces;
 * cv::UMat img = ...;
 * faceDetector.detectFaces(img,facePositions);
   faceDetector.detectFacialFeaures(img,facePositions,faces);
 * \endcode
 *
 * After the above code finishes facePositions will contain a list of bounding boxes representing the locations the detector identified possible faces in the scene and faces
 * will contain a list of Face objects representing the actual faces that were found.  The length of faces will be less than or equal to the length of facePositions; if
 * the first step found a possible face, but no valid features were found within it the second step will not output a zero-feature face.
 *
 * \section coord_sec Coordinate Systems
 *
 * The output of the detectFaces function will provide a series of bounding boxes giving the locations of the possible faces within the image; the origin is in the
 * upper-left corner of the image.
 *
 * The output of the detectFacialFeatures function provides locations of features within each face; i.e. the origin of each feature is the upper-left
 * corner of the face's bounding box, as described above.
 *
 * \section dep_sec Dependencies
 *
 * libface depends on OpenCV 3.0.0, as well as the standard C++11 libraries.
 */

#ifndef FACEDETECT_H
#define FACEDETECT_H

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <cstdio>
#include <vector>
#include <algorithm>

namespace facedetect {
    class Face;

    /*!
     * \brief Class for detecting faces and facial features
     *
     * This class should be instantiated once and used to detect faces in images/video
     */
    class FaceDetect
    {
    public:
        /*!
         * \brief Create a new face detector
         * \param faceCascadePath Path to the OpenCV haar cascade file for face detection
         * \param eyeCascadePath Path to the OpenCV haar cascade file for eye detection
         * \param noseCascadePath Path to the OpenCV haar cascade file for nose detection
         * \param mouthCascadePath Path to the OpenCV haar cascade file for mouth detection
         * \param glassesCascadePath Path to the OpenCV haar cascade file for glasses detection
         */
        FaceDetect(std::string faceCascadePath, std::string eyeCascadePath, std::string noseCascadePath, std::string mouthCascadePath, std::string glassesCascadePath);

        /*!
         * \brief Detect faces within the image
         * \param img The image to process
         * \param faces The positions of the faces detected.  Output parameter
         * \param draw If true, the bounding boxes of the faces are drawn on the frame
         */
        void detectFaces(cv::UMat &img, std::vector<cv::Rect_<int> > &faces, bool draw=false);

        /*!
         * \brief Detect the facial features for a set of face (eyes, nose, mouth)
         * \param img The image to process
         * \param faces The list of face positions within the image
         * \param result The list of faces. Output parameter
         * \param draw If true the facial features are highlighted on the image
         */
        void detectFacialFeaures(cv::UMat &img, const std::vector<cv::Rect_<int> > &faces, std::vector<Face*> &result, bool draw=false);

        /*!
         * \brief Detect facial features for a single face
         *
         * \param img The image to process
         * \param face The position of the face within the image
         * \param eyes The list of possible eyes found in the image
         * \param nose The list of possible noses found in the image
         * \param mouth The list of possible mouths found in the image
         * \param draw If true the facial features are highlighted on the image
         */
        void detectFacialFeaures(cv::UMat &img, const cv::Rect_<int> &face,
                                 std::vector<cv::Rect_<int> > &eyes,
                                 std::vector<cv::Rect_<int> > &nose,
                                 std::vector<cv::Rect_<int> > &mouth,
                                 bool draw=false);

    private:
        std::string faceCascadePath;
        std::string eyeCascadePath;
        std::string noseCascadePath;
        std::string mouthCascadePath;
        std::string glassesCascadePath;

        cv::CascadeClassifier faceClassifier;
        cv::CascadeClassifier eyeClassifier;
        cv::CascadeClassifier noseClassifier;
        cv::CascadeClassifier mouthClassifier;
        cv::CascadeClassifier glassesClassifier;

        /*!
         * \brief Detect eyes in the frame
         * \param roi The region of the image containing the entire face
         * \param eyes The positions of eyes detected (output parameter)
         */
        void detectEyes(cv::UMat &roi, std::vector<cv::Rect_<int> > &eyes);

        /*!
         * \brief Detect the nose in the frame
         * \param roi The region of the image containing the entire face
         * \param eyes The positions of candidate noses detected (output parameter)
         */
        void detectNose(cv::UMat &roi, std::vector<cv::Rect_<int> > &nose);

        /*!
         * \brief Detect the mouth in the frame
         * \param roi The region of the image containing the entire face
         * \param eyes The positions of candidate mouths detected (output parameter)
         */
        void detectMouth(cv::UMat &roi, std::vector<cv::Rect_<int> > &mouth);

        /*!
         * \brief Draw circles on the centres of the eyes
         * \param img The image to draw on; should be the region containing the entire face
         * \param eyes The positions of the features to draw
         */
        void drawEyes(cv::UMat &img, std::vector<cv::Rect_<int> > &eyes);

        /*!
         * \brief Draw a triangle around the nose
         * \param img The image to draw on; should be the region containing the entire face
         * \param eyes The positions of the features to draw
         */
        void drawNose(cv::UMat &img, std::vector<cv::Rect_<int> > &nose);

        /*!
         * \brief Draw a rectangle around the mouth
         * \param img The image to draw on; should be the region containing the entire face
         * \param eyes The positions of the features to draw
         */
        void drawMouth(cv::UMat &img, std::vector<cv::Rect_<int> > &mouth);
    };
}
#endif // FACEDETECT_H
