TEMPLATE = lib
CONFIG -= console
CONFIG -= app_bundle
CONFIG -= qt

TARGET = face

QMAKE_CXXFLAGS += -std=c++11

OBJECTS_DIR = .build/

LIBS += -lopencv_core -lopencv_highgui -lopencv_face -lopencv_ml -lopencv_imgproc -lopencv_video -lopencv_videoio -lopencv_objdetect -lopencv_imgcodecs

SOURCES += facedetect.cpp \
    face.cpp

HEADERS += \
    facedetect.h \
    face.h

HAAR_FILES += \
    haar/eye.xml \
    haar/face.xml \
    haar/left_ear.xml \
    haar/left_eye.xml \
    haar/mouth.xml \
    haar/nose.xml \
    haar/right_ear.xml \
    haar/glasses.xml
    
OTHER_FILES += HAAR_FILES

INSTALLBASE = /usr/local

target.path     = $$INSTALLBASE/lib/
INSTALLS   += target

cfg.files = $$HAAR_FILES
cfg.path = $$INSTALLBASE/share/libface/haar
INSTALLS += cfg

gendoc.commands = doxygen
gendoc.path = $$INSTALLBASE
INSTALLS += gendoc

cpydoc.files = html/*
cpydoc.path = $$INSTALLBASE/doc/libface
INSTALLS += cpydoc

man.commands = $(COPY_DIR) man/* $$INSTALLBASE/man
man.path = $$INSTALLBASE
INSTALLS += man

headers.files = $$HEADERS
headers.path = $$INSTALLBASE/include/facedetect
INSTALLS += headers

postinstall.commands = ldconfig
postinstall.path = $$INSTALLBASE
INSTALLS += postinstall

DISTFILES += \
    Doxyfile
