#include "face.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>

using namespace facedetect;
using namespace std;
using namespace cv;

bool Face::DRAW_BOXES = false;

/*!
 * \brief Compare two rectangles to determine if r1 lies to the left of r2
 * \param r1 The first rectangle
 * \param r2 The second rectangle
 * \return True if r1's centre is left of r2's centre. Otherwise false
 */
static inline bool isLeft(const Rect_<int> &r1, const Rect_<int> &r2)
{
    // compare centres
    return r1.tl().x + r1.width/2 <= r2.tl().x + r2.width;
}

/*!
 * \brief Compare two rectangles to determine if r1 lies above r2
 * \param r1 The first rectangle
 * \param r2 The second rectangle
 * \return True if r1's centre is above r2's centre. Otherwise false.
 */
static inline bool isAbove(const Rect_<int> &r1, const Rect_<int> &r2)
{
    // compare centres
    return r1.tl().y + r1.height/2 <= r2.tl().y + r2.height;
}

/*!
 * \brief Calculate the top-left and bottom-right corners of an array of points
 * \param tl The top-left corner
 * \param br The bottom-right corner
 * \param arr The array of points
 */
static void calculateBoundingBox(Point &tl, Point &br, const vector<Point> &arr)
{
    for(unsigned int i=0; i<arr.size(); i++) {
        if(arr[i].x < tl.x)
            tl.x = arr[i].x;
        if(arr[i].x > br.x)
            br.x = arr[i].x;

        if(arr[i].y < tl.y)
            tl.y = arr[i].y;
        if(arr[i].y > br.y)
            br.y = arr[i].y;
    }
}

//-- CONSTRUCTORS --//

Face::Face()
{
    partial = true;
    sane = false;
    rightEye = NULL;
    leftEye = NULL;
    nose = NULL;
    mouth = NULL;
}

void Face::checkCompletenessAndSanity()
{
    if(leftEye != NULL && rightEye != NULL && nose != NULL && mouth != NULL) {
        partial = false;
    }

    if(!this->partial) {
        if(isAbove(nose->region,mouth->region) &&
           isLeft(rightEye->region,nose->region) &&
           isLeft(nose->region,leftEye->region) &&
           isAbove(leftEye->region,nose->region) &&
           isAbove(rightEye->region,nose->region)) {
            sane = true;
        } else {
            sane = false;
        }
    } else {
        // assume sanity and check every possible insane case
        sane = true;

        if(leftEye != NULL && nose != NULL && sane) {
            sane = isLeft(nose->region,leftEye->region) && isAbove(leftEye->region,nose->region);
        }

        if(rightEye != NULL && nose != NULL && sane) {
            sane = isLeft(rightEye->region,nose->region) && isAbove(rightEye->region,nose->region);
        }

        if(leftEye != NULL && mouth != NULL && sane) {
            sane = isAbove(leftEye->region,mouth->region);
        }

        if(rightEye != NULL && mouth != NULL && sane) {
            sane = isAbove(rightEye->region,mouth->region);
        }

        if(nose != NULL && mouth != NULL && sane) {
            sane = isAbove(nose->region,mouth->region);
        }
    }
}

Face::~Face()
{
    delete leftEye;
    delete rightEye;
    delete nose;
    delete mouth;
}

Face::Feature::Feature()
{
    // do nothing
}

Face::Feature::Feature(const Rect_<int> &region)
{
    this->region = region;
}

Face::Feature::~Feature()
{
}

Face::Eye::Eye(const UMat &face, const Rect_<int> &region) : Feature(region)
{
    pupil = Point(region.tl().x + region.width/2,
                      region.tl().y + region.height/2);

    // for now just assume the corners of the eye are on the edges of the region
    corners[0] = Point(region.tl().x,
                           pupil.y);
    corners[1] = Point(region.br().x,
                           pupil.y);

    // moving up and down from the pupil, find the top and bottom edges
    UMat eye = face(region);
    UMat tmp;
    Point p;
    const int SLICE_SIZE = 3;
    cvtColor(eye.rowRange(0,eye.rows/2).colRange(eye.cols/2-3,eye.cols/2+3),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,0,1,3);
    findBrighest(tmp,p);
    eyelids[0] = Point(region.tl().x + eye.cols/2-SLICE_SIZE + p.x, region.tl().y + p.y);

    cvtColor(eye.rowRange(eye.rows/2,eye.rows-1).colRange(eye.cols/2-SLICE_SIZE,eye.cols/2+SLICE_SIZE),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,0,1,3);
    findBrighest(tmp,p);
    eyelids[1] = Point(region.tl().x + eye.cols/2-SLICE_SIZE + p.x, region.tl().y + eye.rows/2 + p.y);
}

Face::Nose::Nose(const UMat &face, const Rect_<int> &region) : Feature(region)
{
#if 1
    // assume the bridge of the nose is at the top-centre of the region
    // this is normally a safe assumption
    bridge = Point(region.tl().x+region.width/2,
                       region.tl().y);

    UMat nose;
    face(region).copyTo(nose);
    medianBlur(nose,nose,7);
    UMat roi;
    Point p;
    const int SLICE_SIZE = 3;

    // walk along the centre region of the nose looking for the brightest spot
    // we assume that point is the tip of the nose
    roi = nose.rowRange(0,nose.rows-1).colRange(nose.cols/2-SLICE_SIZE,nose.cols/2+SLICE_SIZE);
    findBrighest(roi,p);
    tip = Point(region.tl().x+p.x+nose.cols/2-SLICE_SIZE, region.tl().y+p.y);

    // from the tip we need to find the nostrils
    // we assume that the nostrils will lie approximately in-line with the tip of the nose (slightly above or below)
    roi = nose.rowRange(MAX(0,tip.y-SLICE_SIZE-region.tl().y),MIN(tip.y+SLICE_SIZE-region.tl().y,nose.rows-1)).colRange(0,tip.x-region.tl().x);
    findDarkest(roi,p);
    //circle(roi,p,2,Scalar(0,0,255),-1);
    nostrils[0] = Point(region.tl().x+p.x, tip.y-SLICE_SIZE+p.y);

    roi = nose.rowRange(MAX(0,tip.y-SLICE_SIZE-region.tl().y),MIN(tip.y+SLICE_SIZE-region.tl().y,nose.rows-1)).colRange(tip.x-region.tl().x,nose.cols-1);
    findDarkest(roi,p);
    //circle(roi,p,2,Scalar(0,255,0),-1);
    nostrils[1] = Point(tip.x+p.x, tip.y-SLICE_SIZE+p.y);

    //imshow("nose",nose);
#else
    Point ctr(region.tl().x+region.width/2,
              region.tl().y+region.height/2);

    Rect_<int> left = Rect_<int>(region.tl().x,region.tl().y,region.width/2,region.height*0.9);
    Rect_<int> right = Rect_<int>(ctr.x,region.tl().y,region.width/2,region.height*0.9);

    Rect_<int> top = Rect_<int>(region.tl().x,region.tl().y,region.width,region.height*0.45);
    Rect_<int> bottom = Rect_<int>(region.tl().x,ctr.y,region.width,region.height*0.45);

    Face::findDarkest(face(left),nostrils[0]);
    Face::findDarkest(face(right),nostrils[1]);
    Face::findBrighest(face(top),bridge);
    Face::findBrighest(face(bottom),tip);

    nostrils[0].x += region.tl().x;
    nostrils[0].y += region.tl().y;

    nostrils[1].x += region.tl().x + ctr.x;
    nostrils[1].y += region.tl().y;

    bridge.x += region.tl().x;
    bridge.y += region.tl().y;

    tip.x += region.tl().x;
    tip.y += region.tl().y + ctr.y;
#endif
}

Face::Mouth::Mouth(const UMat &face, const Rect_<int> &region) : Feature(region)
{
    // assume the centre of the mouth is in the middle of the region
    // generally a safe assumption, but maybe we can do better
    centre = Point(region.tl().x + region.width/2,
                   region.tl().y + region.height/2);

    // assume that the corners are dark regions to either side of the centre, up or down a little bit
    UMat mouth;
    face(region).copyTo(mouth);

    Point p;
    UMat tmp;
    const int SLICE_SIZE = 3;
    cvtColor(mouth.colRange(0,mouth.cols/2).rowRange(MAX(0,mouth.rows/2-SLICE_SIZE),MIN(mouth.rows/2+SLICE_SIZE,mouth.rows-1)),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,1,0,3);
    findBrighest(tmp,p);
    corners[0] = Point(region.tl().x + p.x, region.tl().y + mouth.rows/2-SLICE_SIZE + p.y);

    cvtColor(mouth.colRange(mouth.cols/2,mouth.cols-1).rowRange(MAX(0,mouth.rows/2-SLICE_SIZE),MIN(mouth.rows/2+SLICE_SIZE,mouth.rows-1)),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,1,0,3);
    findBrighest(tmp,p);
    corners[1] = Point(region.tl().x + mouth.cols/2 + p.x, region.tl().y + mouth.rows/2-SLICE_SIZE + p.y);

    // do the same thing, but vertically to find the centres of the lips
    cvtColor(mouth.colRange(MAX(0,mouth.cols/2-SLICE_SIZE),MIN(mouth.cols/2+SLICE_SIZE,mouth.cols-1)).rowRange(0,mouth.rows/2),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,0,1,3);
    findBrighest(tmp,p);
    lips[0] = Point(region.tl().x + mouth.cols/2-SLICE_SIZE + p.x, region.tl().y + p.y);

    cvtColor(mouth.colRange(MAX(0,mouth.cols/2-SLICE_SIZE),MIN(mouth.cols/2+SLICE_SIZE,mouth.cols-1)).rowRange(mouth.rows/2,mouth.rows-1),tmp,CV_BGR2GRAY);
    Sobel(tmp,tmp,-1,0,1,3);
    findBrighest(tmp,p);
    lips[1] = Point(region.tl().x + mouth.cols/2-SLICE_SIZE + p.x, region.tl().y + mouth.rows/2 + p.y);

    //imshow("mouth",mouth);
}

Face::Eye::Eye(const Point &pupil, const vector<Point> &corners)
{
    Point tl = Point(pupil.x,pupil.y);
    Point br = Point(pupil.x,pupil.y);
    calculateBoundingBox(tl,br,corners);
    region = Rect_<int> (tl,br);

    this->pupil = Point(pupil.x,pupil.y);
    this->corners[0] = Point(corners[0].x,corners[0].y);
    this->corners[1] = Point(corners[1].x,corners[1].y);

    if(corners.size() >= 4) {
        this->eyelids[0] = Point(corners[2].x,corners[2].y);
        this->eyelids[1] = Point(corners[3].x,corners[3].y);
    } else {
        this->eyelids[0] = Point(this->pupil);
        this->eyelids[1] = Point(this->pupil);
    }
}

Face::Nose::Nose(const Point &bridge, const Point &tip, const vector<Point> &nostrils)
{
    Point tl = Point(bridge.x,bridge.y);
    Point br = Point(tip.x,tip.y);
    calculateBoundingBox(tl,br,nostrils);
    region = Rect_<int> (tl,br);

    this->bridge = Point(bridge.x,bridge.y);
    this->tip = Point(tip.x,tip.y);
    this->nostrils[0] = Point(nostrils[0].x,nostrils[0].y);
    this->nostrils[1] = Point(nostrils[1].x,nostrils[1].y);
}

Face::Mouth::Mouth(const Point &centre, const vector<Point> &corners)
{
    Point tl = Point(centre.x,centre.y);
    Point br = Point(centre.x,centre.y);
    calculateBoundingBox(tl,br,corners);
    region = Rect_<int> (tl,br);

    this->centre = Point(centre.x,centre.y);
    this->corners[0] = Point(corners[0].x,corners[0].y);
    this->corners[1] = Point(corners[1].x,corners[1].y);

    if(corners.size() >= 4) {
        this->lips[0] = Point(corners[2].x,corners[2].y);
        this->lips[1] = Point(corners[3].x,corners[3].y);
    } else {
        this->lips[0] = Point(centre);
        this->lips[1] = Point(centre);
    }
}

//-- FACTORY FUNCTIONS --//

Face *Face::create(const UMat &faceImg,
                   const Rect_<int> &facePosition,
                   vector<Rect_<int> > &eyes,
                   vector<Rect_<int> > &nose,
                   vector<Rect_<int> > &mouth)
{
    Face *face = NULL;

    sort(eyes.begin(),eyes.end(),isLeft);
    sort(nose.begin(),nose.end(),isAbove);
    sort(mouth.begin(),mouth.end(),isAbove);

    if(eyes.size() >= 1 || nose.size() >= 1 || mouth.size() >=1) {
        // we may not have a complete face, but we have at least a few parts
        face = new Face();
        face->position = facePosition;

        if(eyes.size() == 1) {
            if(eyes.front().br().x < facePosition.width/2) {
                face->rightEye = new Eye(faceImg,eyes.front());
            } else {
                face->leftEye = new Eye(faceImg,eyes.front());
            }
        } else if(eyes.size() >= 2) {
            face->leftEye = new Eye(faceImg,eyes.back());
            face->rightEye = new Eye(faceImg,eyes.front());
        }

        if(nose.size() >= 1)
            face->nose = new Nose(faceImg,nose.back());

        if(mouth.size() >= 1)
            face->mouth = new Mouth(faceImg,mouth.back());

        face->checkCompletenessAndSanity();
    }

    return face;
}

static void offsetPoint(Point &dst, const Point &src, const Point &tl) {
    dst.x = src.x - tl.x;
    dst.y = src.y - tl.y;
}

Face *Face::create(const Point &tl, const Point &br,
                   const vector<Point> &leftEye,
                   const vector<Point> &rightEye,
                   const vector<Point> &nose,
                   const vector<Point> &mouth)
{
    // create the face we're going to return
    Face *face = new Face();

    // get the position of the face in the image
    face->position = Rect_<int>(tl,br);

    vector<Point> corners;
    corners.push_back(Point(0,0));
    corners.push_back(Point(0,0));
    Point p,q;

    offsetPoint(corners[0],leftEye[0],tl);
    offsetPoint(corners[1],leftEye[2],tl);
    offsetPoint(p,leftEye[1],tl);
    face->leftEye = new Eye(p,corners);

    offsetPoint(corners[0],rightEye[0],tl);
    offsetPoint(corners[1],rightEye[2],tl);
    offsetPoint(p,rightEye[1],tl);
    face->rightEye = new Eye(p,corners);

    offsetPoint(corners[0],nose[2],tl);
    offsetPoint(corners[1],nose[3],tl);
    offsetPoint(p,nose[0],tl);
    offsetPoint(q,nose[1],tl);
    face->nose = new Nose(p,q,corners);

    offsetPoint(corners[0],mouth[0],tl);
    offsetPoint(corners[1],mouth[2],tl);
    offsetPoint(p,mouth[1],tl);
    face->mouth = new Mouth(p,corners);

    face->checkCompletenessAndSanity();

    return face;
}


//-- DRAWING FUNCTIONS --//

void Face::draw(UMat &img, bool points, bool mesh, bool applyOffset) const
{
    UMat canvas;

    if(applyOffset)
    {
        canvas = img(position);
    }
    else
    {
        canvas = img;
    }

    if(mesh) {
        drawMesh(canvas);
    }

    if(points) {
        if(leftEye != NULL)
            leftEye->draw(canvas);
        if(rightEye != NULL)
            rightEye->draw(canvas);
        if(nose != NULL)
            nose->draw(canvas);
        if(mouth != NULL)
            mouth->draw(canvas);
    }
}

void Face::copyFeatures(UMat &dst, const UMat &face) const
{
    if(leftEye != NULL) {
        UMat tmp = face(leftEye->region);
        tmp.copyTo(dst(leftEye->region));
        imshow("left eye",tmp);
    }

    if(rightEye != NULL) {
        UMat tmp = face(rightEye->region);
        tmp.copyTo(dst(rightEye->region));
        imshow("right eye",tmp);
    }

    if(nose != NULL) {
        UMat tmp = face(nose->region);
        tmp.copyTo(dst(nose->region));
        imshow("nose",tmp);
    }

    if(mouth != NULL) {
        UMat tmp = face(mouth->region);
        tmp.copyTo(dst(mouth->region));
        imshow("mouth",tmp);
    }
}

void Face::drawMesh(UMat &img) const
{
    Scalar lineColour = Scalar(255,0,0); // blue if everything is fine

    if(!sane)
        lineColour = Scalar(0,0,255); // red if the face is not sane
    else if(partial)
        lineColour = Scalar(0,255,255); // yellow for partials

    const int thickness = 1;

    // connect the eye points
    if(leftEye != NULL) {
        line(img,leftEye->corners[0],leftEye->eyelids[0],lineColour,thickness);
        line(img,leftEye->corners[0],leftEye->eyelids[1],lineColour,thickness);
        line(img,leftEye->corners[1],leftEye->eyelids[0],lineColour,thickness);
        line(img,leftEye->corners[1],leftEye->eyelids[1],lineColour,thickness);
    }
    if(rightEye != NULL) {
        line(img,rightEye->corners[0],rightEye->eyelids[0],lineColour,thickness);
        line(img,rightEye->corners[0],rightEye->eyelids[1],lineColour,thickness);
        line(img,rightEye->corners[1],rightEye->eyelids[0],lineColour,thickness);
        line(img,rightEye->corners[1],rightEye->eyelids[1],lineColour,thickness);
    }

    // draw the nose
    if(nose != NULL) {
        //line(img,nose->bridge,nose->tip,lineColour,thickness);
        line(img,nose->bridge,nose->nostrils[0],lineColour,thickness);
        line(img,nose->bridge,nose->nostrils[1],lineColour,thickness);
        line(img,nose->tip,nose->nostrils[0],lineColour,thickness);
        line(img,nose->tip,nose->nostrils[1],lineColour,thickness);
    }

    // draw the mouth
    if(mouth != NULL) {
        line(img,mouth->centre,mouth->corners[0],lineColour,thickness);
        line(img,mouth->centre,mouth->corners[1],lineColour,thickness);
        line(img,mouth->corners[0],mouth->lips[0],lineColour,thickness);
        line(img,mouth->corners[0],mouth->lips[1],lineColour,thickness);
        line(img,mouth->corners[1],mouth->lips[0],lineColour,thickness);
        line(img,mouth->corners[1],mouth->lips[1],lineColour,thickness);
    }

    // draw connecting lines between the features
    if(nose != NULL && mouth != NULL) {
        line(img,nose->nostrils[0],mouth->corners[0],lineColour,thickness);
        line(img,nose->nostrils[1],mouth->corners[1],lineColour,thickness);
        line(img,nose->tip,mouth->lips[0],lineColour,thickness);
    }
    if(leftEye != NULL && nose != NULL) {
        line(img,leftEye->corners[0],nose->bridge,lineColour,thickness);
        line(img,leftEye->corners[1],nose->nostrils[1],lineColour,thickness);
    }
    if(rightEye != NULL && nose != NULL) {
        line(img,rightEye->corners[1],nose->bridge,lineColour,thickness);
        line(img,rightEye->corners[0],nose->nostrils[0],lineColour,thickness);
    }


}

void Face::Eye::draw(UMat &img, Point offset) const
{
    Point c(pupil.x+offset.x,
                pupil.y+offset.y);
    Point l(corners[0].x+offset.x,
            corners[0].y+offset.y);
    Point r(corners[1].x+offset.x,
            corners[1].y+offset.y);
    Point t(eyelids[0].x+offset.x,
            eyelids[0].y+offset.y);
    Point b(eyelids[1].x+offset.x,
            eyelids[1].y+offset.y);

    Point tl(region.tl().x+offset.x,
                 region.tl().y+offset.y);
    Point br(region.br().x+offset.x,
                 region.br().y+offset.y);

    circle(img,l,3,Scalar(0,0,255),-1);
    circle(img,r,3,Scalar(0,255,0),-1);
    circle(img,t,3,Scalar(0,255,255),-1);
    circle(img,b,3,Scalar(255,255,0),-1);
    circle(img,c,3,Scalar(255,0,0),-1);

    if(DRAW_BOXES)
        rectangle(img,tl,br,Scalar(255,255,255),1);
}

void Face::Nose::draw(UMat &img, Point offset) const
{
    Point a(bridge.x+offset.x,
                bridge.y+offset.y);
    Point b(tip.x+offset.x,
                tip.y+offset.y);
    Point c(nostrils[0].x+offset.x,
                nostrils[0].y+offset.y);
    Point d(nostrils[1].x+offset.x,
                nostrils[1].y+offset.y);

    Point tl(region.tl().x+offset.x,
                 region.tl().y+offset.y);
    Point br(region.br().x+offset.x,
                 region.br().y+offset.y);

#if 0
    Scalar colour(0,255,0);
    line(img,a,b,colour,1);
    line(img,c,d,colour,1);
    line(img,a,c,colour,1);
    line(img,a,d,colour,1);
    line(img,b,c,colour,1);
    line(img,b,d,colour,1);
#endif

    circle(img,a,3,Scalar(0,255,255),-1);
    circle(img,b,3,Scalar(255,255,0),-1);
    circle(img,c,3,Scalar(0,0,255),-1);
    circle(img,d,3,Scalar(0,255,0),-1);

    if(DRAW_BOXES)
        rectangle(img,tl,br,Scalar(255,255,255),1);
}

void Face::Mouth::draw(UMat &img, Point offset) const
{
    // mark the mouth as line
    Point l(corners[0].x+offset.x,
                corners[0].y+offset.y);
    Point r(corners[1].x+offset.x,
                corners[1].y+offset.y);
    Point c(centre.x+offset.x,
            centre.y+offset.y);

    Point t(lips[0].x+offset.x,
            lips[0].y+offset.y);

    Point b(lips[1].x+offset.x,
            lips[1].y+offset.y);

    Point tl(region.tl().x+offset.x,
                 region.tl().y+offset.y);
    Point br(region.br().x+offset.x,
                 region.br().y+offset.y);

#if 0
    line(img,l,r,Scalar(255,0,0),1);
#endif
    circle(img,l,3,Scalar(0,0,255),-1);
    circle(img,r,3,Scalar(0,255,0),-1);
    circle(img,t,3,Scalar(0,255,255),-1);
    circle(img,b,3,Scalar(255,255,0),-1);
    circle(img,c,3,Scalar(255,0,0),-1);

    if(DRAW_BOXES)
        rectangle(img,tl,br,Scalar(255,255,255),1);
}

//-- UTILITIES, ETC --//

void Face::findBrighest(const UMat &img, Point &pt)
{
    UMat gray;
    if(img.channels() > 1) {
        cvtColor(img,gray,CV_BGR2GRAY);
    } else {
        img.copyTo(gray);
    }
    GaussianBlur(gray,gray,Size(3,3),1.0);
    minMaxLoc(gray,NULL,NULL,NULL,&pt);
}

void Face::findDarkest(const UMat &img, Point &pt)
{
    UMat inverse;
    bitwise_not(img,inverse);
    findBrighest(inverse,pt);
}

void Face::getPoints(std::vector<Point> &points, bool applyOffset) const
{
    points.clear();
    points.push_back(leftEye->pupil);
    points.push_back(leftEye->corners[0]);
    points.push_back(leftEye->corners[1]);
    points.push_back(rightEye->pupil);
    points.push_back(rightEye->corners[0]);
    points.push_back(rightEye->corners[1]);
    points.push_back(nose->bridge);
    points.push_back(nose->tip);
    points.push_back(nose->nostrils[0]);
    points.push_back(nose->nostrils[1]);
    points.push_back(mouth->centre);
    points.push_back(mouth->corners[0]);
    points.push_back(mouth->corners[1]);

    if(applyOffset)
    {
        for_each(points.begin(),points.end(),[&](Point &p){
            p.x += this->position.tl().x;
            p.y += this->position.tl().y;
        });
    }
}
