#ifndef FACE_H
#define FACE_H

#include <opencv2/core.hpp>
#include <vector>

namespace facedetect {
    /*!
     * \brief Class representing a single face, containing two eyes, a nose, and a mouth
     */
    class Face
    {
    public:
        /*!
         * \brief A facial feature contained within the face.
         * Serves as abstract superclass for Eye, Nose, and Mouth classes.
         *
         * All corrdinates contained within Feature subclasses have their origin at the upper-left corner
         * of the face (see Face constructor)
         */
        class Feature {
        public:
            Feature(const cv::Rect_<int> &region);
            virtual ~Feature();
            cv::Rect_<int> region;

            /*!
             * \brief Draw the feature onto an image
             * \param img The image to draw upon; assumed to have (0,0) located on the top-left corner of the face
             * \param offset Optional offset for where to draw the feature
             */
            virtual void draw(cv::UMat &img, cv::Point offset=cv::Point(0,0)) const = 0;

        protected:
            Feature();
        };

        /*!
         * \brief A single eye (either left or right)
         */
        class Eye : public Feature {
        public:
            /*!
             * \brief Create a new eye, dynamically calculating where the corners are within the face
             * \param face The image of the entire face
             * \param region The bounding box of the eye within the face
             */
            Eye(const cv::UMat &face, const cv::Rect_<int> &region);

            /*!
             * \brief Create a new eye with specific, pre-defined positions
             * \param pupil The location of the pupil within the face
             * \param corners The location of the corners of the eye in order left, right, top, bottom within the image.  Must contain at least 2 points, anything after index 3 is ignored
             */
            Eye(const cv::Point &pupil, const std::vector<cv::Point> &corners);

            /*!
             * \brief Location of the pupil within the face
             */
            cv::Point pupil;

            /*!
             * \brief Location of the corners of the eye within the face.
             *
             * Index 0 is the leftmost corner (image left, person right).  Index 1 is the rightmost corner (image right, person left)
             */
            cv::Point corners[2];

            /*!
             * \brief Location of the centre top and bottom of the eyelids within the face
             *
             * Index 0 is the upper eyelid, index 1 is the lower eyelid
             */
            cv::Point eyelids[2];

            virtual void draw(cv::UMat &img, cv::Point offset=cv::Point(0,0)) const;
        };

        /*!
         * \brief A nose
         */
        class Nose : public Feature {
        public:
            /*!
             * \brief Create a new nose, dynamically calculating where the bridge, tip, and nostrils are based on the image data provided
             * \param face The image of the entire face
             * \param region The region within the face where the nose is
             */
            Nose(const cv::UMat &face, const cv::Rect_<int> &region);

            /*!
             * \brief Create a new nose with pre-defined bridge, tip, and nostril positions
             * \param bridge The position of the bridge of the nose within the face
             * \param tip The position of the tip of the nose within the face
             * \param nostrils The positions of the nostrils/narins within the face.  Must contain at least 2 points, any points past index 1 are ignored.
             */
            Nose(const cv::Point &bridge, const cv::Point &tip, const std::vector<cv::Point> &nostrils);

            /*!
             * \brief The positions of the nostrils within the face.
             * Index 0 is the leftmost nostril (image left, person right), index 1 is the rightmost nostril (image right, person left)
             */
            cv::Point nostrils[2];

            /*!
             * \brief The position of the tip of the nose
             */
            cv::Point tip;

            /*!
             * \brief The position of the bridge of the nose
             */
            cv::Point bridge;

            virtual void draw(cv::UMat &img, cv::Point offset=cv::Point(0,0)) const;
        };

        /*!
         * \brief A mouth
         */
        class Mouth : public Feature {
        public:
            /*!
             * \brief Create a new mouth, dynamically calculating where the centre and corners are within the face
             * \param face The image of the entire face
             * \param region The region within the face where the mouth is located
             */
            Mouth(const cv::UMat &face, const cv::Rect_<int> &region);

            /*!
             * \brief Create a new mouth with pre-defined corners and centre
             * \param centre The centre of the mouth within the face
             * \param corners The corners of the mouth in order left, right, top, bottom (within the image). Must contain at least 2 items, anything after index 3 is ignored
             */
            Mouth(const cv::Point &centre, const std::vector<cv::Point> &corners);

            /*!
             * \brief The corners of the mouth from left to right within the image
             */
            cv::Point corners[2];

            /*!
             * \brief The centres of the upper [0] and lower [1] lips within the image
             */
            cv::Point lips[2];

            /*!
             * \brief The position of the centre of the mouth
             */
            cv::Point centre;

            virtual void draw(cv::UMat &img, cv::Point offset=cv::Point(0,0)) const;
        };

        /*!
         * \brief Create a new instance of Face based on the outputs of the facial detection Haar cascade filters
         * \param face The image of the entire face (but only the face)
         * \param facePosition The position of the face within its surrounding image
         * \param eyes The positions of possible eyes detected within the face
         * \param nose The positions of possible noses detected within the face
         * \param mouth The positions of possible mouths detected within the face
         * \return A new instance of Face, or null if the provided features were invalid.
         */
        static Face *create(const cv::UMat &face,
                            const cv::Rect_<int> &facePosition,
                            std::vector<cv::Rect_<int>> &eyes,
                            std::vector<cv::Rect_<int>> &nose,
                            std::vector<cv::Rect_<int>> &mouth);

        /*!
         * \brief Create a new instance of Face with specific pre-defined features
         * \param tl The top-left corner of the face within its surrounding image
         * \param br The bottom-right corner of the face within its surrounding image
         * \param leftEye The face's left eye
         * \param rightEye The face's right eye
         * \param nose The face's nose
         * \param mouth The face's mouth
         * \return A new instance of Face using copies of the provided features
         */
        static Face *create(const cv::Point &tl, const cv::Point &br,
                            const std::vector<cv::Point> &leftEye,
                            const std::vector<cv::Point> &rightEye,
                            const std::vector<cv::Point> &nose,
                            const std::vector<cv::Point> &mouth);

        /*! \brief Destructor */
        ~Face();

        /*!
         * \brief The face's position within the surrounding image
         */
        cv::Rect_<int> position;

        /*!
         * \brief The face's left eye (will appear in the right area of the image)
         */
        Eye *leftEye;

        /*!
         * \brief The face's right eye (will appear in the left area of the image)
         */
        Eye *rightEye;

        /*!
         * \brief The face's nose
         */
        Nose *nose;

        /*!
         * \brief The face's mouth
         */
        Mouth *mouth;

        /*!
         * \brief If true we draw bounding boxes around the features
         */
        static bool DRAW_BOXES;

        /*!
         * \brief Draw the face points and/or mesh onto an image
         *
         * Each feature consists of 4-5 points, colour-coded as follows:
         * * Top: yellow
         * * Bottom: cyan
         * * Left: red (image left, so technically the right side of the person)
         * * Right: green (image right, so technically the left side of the person)
         * * Centre: blue (eyes and mouth only, not used for nose)
         *
         * Mesh line colours are similarly colour-coded:
         * * Complete, sane face: blue
         * * Incomplete, sane face: yellow
         * * Corrupt face: red
         *
         * A face is considered sane if the eyes are above and to each side of the nose, and the nose is above the mouth.
         *
         * A face us considered complete if it has exactly two eyes, one nose, and one mouth.
         *
         * \see isSane
         * \see isComplete
         *
         * \param img The image to draw on
         * \param points If true all the face keypoints are marked with circles
         * \param mesh If true the face keypoints are connected by lines
         * \param applyOffset If true, we draw the face in a position corresponding to \see position.  Otherwise
         *        we draw from (0,0)
         */
        void draw(cv::UMat &img, bool points=true, bool mesh=true, bool applyOffset=false) const;

        /*!
         * \brief Copy the facial features from the picture of the whole face to a destination image
         * \param dst The image to draw on
         * \param face The image of the entire face
         */
        void copyFeatures(cv::UMat &dst, const cv::UMat &face) const;

        /*!
         * \brief Collect all of the keypoints on the face into a single list.  Points are in the following order:
         * * right eye corner0
         * * right pupil
         * * right eye corner1
         * * left eye corner
         * * left pupil
         * * left eye corner1
         * * nose bridge
         * * nose tip
         * * nose corner0
         * * nose corner1
         * * mouth corner0
         * * mouth centre
         * * mouth corner1
         * \param points The points of the face; output parameter
         * \param applyOffset If true, all points are shifted in position according to position. \see position
         */
        void getPoints(std::vector<cv::Point> &points,bool applyOffset=true) const;

        /*!
         * \brief Is this a partial face (i.e. is it missing some or all features)
         * \return True if the face is not missing any features
         */
        bool isComplete() const {return !partial;}

        /*!
         * \brief Is the face sane (i.e. are the features aligned in a reasonable way)
         * \return True if the face's features are sanely positioned
         */
        bool isSane() const {return sane;}

    private:
        /*!
         * \brief Create a new face with null pointers for all features
         */
        Face();

        /*!
         * \brief Check the alignment of facial features to make sure they're sanely positioned and make sure we have all the features
         * Sets the values of sane and partial
         */
        void checkCompletenessAndSanity();

        /*!
         * \brief Draw the face mesh onto an image.  We draw relative to (0,0)
         * \param img The image on which to draw
         */
        void drawMesh(cv::UMat &img) const;

        /*!
         * \brief Find the brightest single pixel within an image
         * \param img The image to analyse
         * \param pt The location of the brightest point; output parameter
         */
        static void findBrighest(const cv::UMat &img, cv::Point &pt);

        /*!
         * \brief Find the darkest single pixel within an image
         * \param img The image to analyse
         * \param pt The location of the darkest point; output parameter
         */
        static void findDarkest(const cv::UMat &img, cv::Point &pt);

        /*!
         * \brief Indicates if this is a partial face or not (based on the input to the factory function \see create
         */
        bool partial;

        /*!
         * \brief Indicates of the facial features are sanely positioned (i.e. mouth below nose, eyes on either side of nose, etc)
         */
        bool sane;
    };
}
#endif // FACE_H
